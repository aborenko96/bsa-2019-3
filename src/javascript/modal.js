import {fighterService} from './services/fightersService'

class Modal {

    constructor() {
        this.closeButton.addEventListener("click", this.toggleModal.bind(this));
        this.submitButton.addEventListener("click", this.submitData.bind(this));
    }

    modal = document.querySelector(".modal");
    closeButton = document.querySelector(".close-button");
    nameInput = document.querySelector("#fighter-name");
    healthInput = document.querySelector("#fighter-health");
    attackInput = document.querySelector("#fighter-attack");
    defenseInput = document.querySelector("#fighter-defense");
    idInput = document.querySelector("#fighter-id");
    submitButton = this.modal.querySelector("button");

    toggleModal() {
        this.modal.classList.toggle("show-modal");
    }

    submitData() {
        let data = {
            health: this.healthInput.value,
            attack: this.attackInput.value,
            defense: this.defenseInput.value
        };

        fighterService.setFighterDetails(this.idInput.value, data)
            .then(() => {
                this.toggleModal();
                alert('Fighter data has been successfully saved');
            })
            .catch(() => {
                alert('Error while saving fighter data');
            });
    }
}

export const modal = new Modal();