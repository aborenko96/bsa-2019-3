import {fighterService} from '../services/fightersService';
import Fighter from '../fighter';

class FightersStorage {
    fightersMap = new Map();

    async getFighterFromStorage(_id) {
        if (!this.fightersMap.has(_id)) {
            const data = await fighterService.getFighterDetails(_id);
            this.fightersMap.set(_id, new Fighter(data.name, data.health, data.attack, data.defense));
        }

        return this.fightersMap.get(_id);
    }


}

export const fightersStorage = new FightersStorage();