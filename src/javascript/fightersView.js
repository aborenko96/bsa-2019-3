import View from './view';
import FighterView from './fighterView';
import {modal} from "./modal";
import {fightersStorage} from "./storages/fightersStorage";

class FightersView extends View {
    constructor(fighters) {
        super();

        this.handleClick = this.handleFighterClick.bind(this);
        this.createFighters(fighters);
    }

    fightersDetailsMap = new Map();

    createFighters(fighters) {
        const fighterElements = fighters.map(fighter => {
            const fighterView = new FighterView(fighter, this.handleClick);
            return fighterView.element;
        });

        this.element = this.createElement({tagName: 'div', className: 'fighters'});
        this.element.append(...fighterElements);
    }

    async handleFighterClick(event, fighter) {
        this.fightersDetailsMap.set(fighter._id, fighter);

        let details = await fightersStorage.getFighterFromStorage(fighter._id);

        modal.nameInput.innerText = details.name;
        modal.healthInput.value = details.health;
        modal.attackInput.value = details.attack;
        modal.defenseInput.value = details.defense;
        modal.idInput.value = fighter._id;

        modal.toggleModal();

        // get from map or load info and add to fightersMap
        // show modal with fighter info
        // allow to edit health and power in this modal
    }
}

export default FightersView;