import {getRandomInt} from "./helpers/mathHelper";

class Fighter {
    constructor(name, health, attack, defense) {
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.defense = defense;
    }

    name;
    health;
    attack;
    defense;
    enemy = null;

    setEnemy(enemy) {
        this.enemy = enemy;
    }

    getHitPower() {
        return this.attack * getRandomInt(1,3);
    }

    getBlockPower() {
        return this.defense * getRandomInt(1,3);
    }

    getDamage() {
        return Math.max(0,  this.getHitPower() - this.getBlockPower());
    }
}

export default Fighter;