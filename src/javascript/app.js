import FightersView from './fightersView';
import View from './view';
import {fighterService} from './services/fightersService';
import {getRandomInt} from "./helpers/mathHelper";
import {fightersStorage} from "./storages/fightersStorage";
import Fighter from "./fighter";

class App {
    constructor() {
        this.startApp();
    }

    static rootElement = document.getElementById('root');
    static loadingElement = document.getElementById('loading-overlay');

    static view = new View();

    static firstPlayerHealth = App.view.createElement({
        tagName: 'p',
        className: 'health',
        attributes: {id: 'health-1'}
    });

    static secondPlayerHealth = App.view.createElement({
        tagName: 'p',
        className: 'health',
        attributes: {id: 'health-2'}
    });

    static fightLog = App.view.createElement({
        tagName: 'div',
        className: 'log'
    });

    static isFightStarted = false;

    async createPlayer(id) {
        let data = await fightersStorage.getFighterFromStorage(id);

        return new Fighter(data.name, data.health, data.attack, data.defense);
    }

    clear() {
       App.fightLog.innerHTML = '';

       document.querySelectorAll('.cloned').forEach(element => {
           element.remove();
       });

       document.querySelectorAll('.hidden').forEach(element => {
           element.classList.remove('hidden');
       });

       App.isFightStarted = false;
    }

    log(message) {
        let record = App.view.createElement({
            tagName: 'p',
            className: 'record'
        });

        record.innerText = message;

        App.fightLog.prepend(record);
    }
    attack(firstPlayer, secondPlayer, attacker) {
        let damage = attacker.getDamage();

        let info = (attacker === firstPlayer ? 'Fighter 1' : 'Fighter 2') + ` (${attacker.name})`;

        attacker.enemy.health -= damage;

        this.log(info + `: ${damage}`);

        App.firstPlayerHealth.innerText = Math.max(Number(firstPlayer.health), 0);
        App.secondPlayerHealth.innerText = Math.max(Number(secondPlayer.health), 0);

        if (attacker.enemy.health <= 0) {
            this.log(info + " win");
            setTimeout(this.clear, 3000);
            return;
        }

        attacker = attacker.enemy;

        setTimeout(() => {
            this.attack(firstPlayer, secondPlayer, attacker)
        }, 1000);
    }

    async preparePlayer(elementId, healthElement) {
        let id = document.getElementById(elementId).value;

        let player = await this.createPlayer(id);

        App.prepareFighterView(id, healthElement);

        return player;
    }

    static prepareFighterView(id, healthElement) {
        let fightersContainer = document.querySelector('.fighters');
        let view = fightersContainer.querySelector(`.fighter:nth-child(${id})`).cloneNode(true);

        view.classList.add('cloned');
        view.prepend(healthElement);

        fightersContainer.appendChild(view);
    }

    async fight() {

        const firstPlayer = await this.preparePlayer('player-1', App.firstPlayerHealth);
        const secondPlayer = await this.preparePlayer('player-2', App.secondPlayerHealth);

        App.firstPlayerHealth.innerText = firstPlayer.health;
        App.secondPlayerHealth.innerText = secondPlayer.health;

        document.querySelectorAll('.fighter:not(.cloned), .fighter-selectors, .start-fight').forEach(element => {
            element.classList.add('hidden');
        });

        let attacker = getRandomInt(0, 2) ? firstPlayer : secondPlayer;

        firstPlayer.setEnemy(secondPlayer);
        secondPlayer.setEnemy(firstPlayer);

        setTimeout(() => {
            this.attack(firstPlayer, secondPlayer, attacker)
        }, 1000);
    }

    async startApp() {
        try {
            App.loadingElement.style.visibility = 'visible';

            const fighters = await fighterService.getFighters();

            const fightersView = new FightersView(fighters);
            const fightersElement = fightersView.element;

            App.rootElement.appendChild(fightersElement);

            const view = new View();
            const fightersSelect = view.createElement({tagName: 'div', className: 'fighter-selectors'});

            const fightersOptions = fighters.map(fighter => {
                let option = view.createElement({
                    tagName: 'option',
                    className: 'fighter-option',
                    attributes: {value: fighter._id}
                });

                option.innerText = fighter.name;

                return option;
            });


            const firstPlayerSelector = view.createElement({
                tagName: 'select',
                className: 'fighter-select',
                attributes: {id: 'player-1'}
            });

            const startButton = view.createElement({
                tagName: 'button',
                className: 'start-fight',
                attributes: {}
            });

            startButton.innerText = "Start fight";
            startButton.addEventListener('click', async () => {
                if (App.isFightStarted) {
                    alert('Fight is already started');
                    return;
                }

                App.isFightStarted = true;
                await this.fight();

            });

            firstPlayerSelector.append(...fightersOptions);

            const secondPlayerSelector = firstPlayerSelector.cloneNode(true);

            secondPlayerSelector.id = 'player-2';

            fightersSelect.append(firstPlayerSelector, secondPlayerSelector);

            App.rootElement.appendChild(fightersSelect);
            App.rootElement.appendChild(startButton);
            App.rootElement.appendChild(App.fightLog);

        } catch (error) {
            console.warn(error);
            App.rootElement.innerText = 'Failed to load data';
        } finally {
            App.loadingElement.style.visibility = 'hidden';
        }
    }
}

export default App;